Autor: Vandré Augusto Viegas Lopes
Projeto: Sistema de gerenciamento e pesquisa de banco de dados de livraria. v1.0

Projeto destinado ao processo seletivo Trainees Memory 2/2019.

Tecnologias usadas:
	back-end(servidor): NodeJS e Express;
	banco de dados: MySQL;
	front-end(interface): Handlebars, HTML e CSS.