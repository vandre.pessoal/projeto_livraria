//VARIÁVEIS GLOBAIS DO JSON DE TROCA DE INFORMAÇÃO COM OS HANDLEBARS
var tipo, mensagem, dado, comando;

//IMPORT DE CLASSES E UTILITARIOS CRIADOS
const autor = require("./public/classes/autor");
const livro = require("./public/classes/livro");
const editora = require("./public/classes/editora");
const SQL = require("./public/classes/comandos_sql");

//IMPORT DO EXPRESS, BODY-PARSER e HANDLEBARS
const express = require("express");
const handlebars = require("express-handlebars");
const bodyParser = require("body-parser");
const app = express();

//CONFIGURAR MYSQL
const mysql = require("mysql");
var banco, mSQL;

function conectar_banco(nome_banco){
  //mysql -h localhost -u root -p
  //use livraria; 
  banco = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "Akalanata*99",
    database : nome_banco
  });
    
  banco.connect(function(err) {
    if (err) {console.log("Falha na conexão com o banco de dados " + nome_banco + ": "); throw err;}
    console.log("Conexão com o banco de dados \"" + nome_banco + "\" com MySQL realizada com sucesso!");
    mSQL = new SQL(banco);
  })
}

//CONFIGURAR HANDLEBARS E BODY-PARSER 
app.engine("handlebars", handlebars({defaultLayout: "main"}));
app.set("view engine", "handlebars");
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());
app.use('/css',express.static('public/css'));

/*
-------------------------------------------------------------------------------------------------------------------------
                        DEFINIÇÃO E PROGRAMAÇÃO DAS ROTAS DE NAVEGAÇÃO DAS PÁGINAS 
-------------------------------------------------------------------------------------------------------------------------
*/
//ROTAS GET
app.get('/texto', function(req, res){
  res.json({
    tipo: tipo,
    texto: mensagem,
    dados: dado,
    comando: comando
  })
})

app.get("/", function(req, res){
  res.render("index");
});

app.get("/autor", function(req, res){
  res.render("form_autor");
});

app.get("/livro", function(req, res){
  res.render("form_livro");
});

app.get("/editora", function(req, res){
  res.render("form_editora");
});

app.get("/pesq_autor", function(req, res){
  res.render("form_autor_pesq");
});

app.get("/pesq_livro", function(req, res){
  res.render("form_livro_pesq");
});

app.get("/pesq_editora", function(req, res){
  res.render("form_editora_pesq");
});

app.get("/pesq_livro_autor", function(req, res){
  res.render("form_livro_pesq_autor");
});

app.get("/pesq_livro_editora", function(req, res){
  res.render("form_livro_pesq_editora");
});


//ROTAS POST DE AUTOR: PESQUISA POR NOME
app.post("/select_autor", function(req, res){
  if(!req.body.nome){
    tipo = 1;
    mensagem = "Campo de nome de autor não pode ficar em branco."; 
    res.render("fim"); 
  }
  else{
    mSQL.autor_pesquisar_nome(req.body.nome, function(resultado, campos, comando_sql){
      comando = comando_sql;
      tipo = 2;
      mensagem = "Resultado da pesquisa do autor \"" + req.body.nome + "\":";
      dado = JSON.stringify(resultado);
      res.render("fim"); 
    });
  }
});

//ROTAS POST DE AUTOR: REMOÇÃO POR ID
app.post("/remove_autor", function(req, res){
  if(!req.body.id){
    tipo = 1;
    mensagem = "Campo de id de autor não pode ficar em branco."; 
    res.render("fim"); 
  }
  else{
    mSQL.autor_pesquisar_id(req.body.id, function(resultado, campos, comando_sql){
      comando = comando_sql;
      if(JSON.stringify(resultado) == '[]'){
          tipo = 1;
          mensagem = "Autor de id " + req.body.id + " não encontrado no banco de dados.";
      }
      else{
        comando = mSQL.autor_remover(req.body.id);
        tipo = 0;
        mensagem = "Remoção do autor de id " + req.body.id + " realizada com sucesso!";
      }
      res.render("fim");
    });
  }
});

//ROTAS POST DE AUTOR: INSERÇÃO POR DADOS
app.post("/insert_autor", function(req, res){
  var a = new autor(-1, req.body.nome, req.body.biografia);
  if(!a.valida()){
    tipo = 1;
    mensagem = "Campo de nome de autor não pode ficar em branco.";  
  }
  else{
    comando = mSQL.autor_incluir(a);
    console.log(comando);
    tipo = 0;
    mensagem = "Inserção do autor de nome " + req.body.nome + " realizada com sucesso!";
  }
  res.render("fim");
});

//ROTAS POST DE AUTOR: ALTERAÇÃO POR ID + DADOS
app.post("/alter_autor", function(req, res){
  if(!req.body.id){
    tipo = 1;
    mensagem = "Campo de id de autor não pode ficar em branco.";
    res.render("fim"); 
  }
  else{
    mSQL.autor_pesquisar_id(req.body.id, function(resultado, campos, comando_sql){
      comando = comando_sql;
      var a = new autor(-1, req.body.nome, req.body.biografia);
      if(!a.valida()){
        tipo = 1;
        mensagem = "Campo de nome de autor não pode ficar em branco.";  
      }
      else if(JSON.stringify(resultado) == '[]'){
        tipo = 1;
        mensagem = "Autor de id " + req.body.id + " não encontrado no banco de dados.";  
      }
      else{
        comando = mSQL.autor_alterar(a, req.body.id);
        tipo = 0;
        mensagem = "Alteração dos dados do autor de id " + req.body.id + " realizada com sucesso!";
      }
      res.render("fim");
    });
  }
});


//ROTAS POST DE EDITORA: PESQUISA POR NOME
app.post("/select_editora", function(req, res){
  if(!req.body.nome){
    tipo = 1;
    mensagem = "Campo de nome de editora não pode ficar em branco."; 
    res.render("fim"); 
  }
  else{
    mSQL.editora_pesquisar_nome(req.body.nome, function(resultado, campos, comando_sql){
      comando = comando_sql;
      tipo = 3;
      mensagem = "Resultado da pesquisa da editora \"" + req.body.nome + "\":";
      dado = JSON.stringify(resultado);
      res.render("fim"); 
    });
  }
});

//ROTAS POST DE EDITORA: REMOÇÃO POR ID
app.post("/remove_editora", function(req, res){
  if(!req.body.id){
    tipo = 1;
    mensagem = "Campo de id de editora não pode ficar em branco."; 
    res.render("fim"); 
  }
  else{
    mSQL.editora_pesquisar_id(req.body.id, function(resultado, campos, comando_sql){
      comando = comando_sql;
      if(JSON.stringify(resultado) == '[]'){
          tipo = 1;
          mensagem = "Editora de id " + req.body.id + " não encontrada no banco de dados.";
      }
      else{
        comando = mSQL.editora_remover(req.body.id);
        tipo = 0;
        mensagem = "Remoção da editora de id " + req.body.id + " realizada com sucesso!";
      }
      res.render("fim");
    });
  }
});

//ROTAS POST DE EDITORA: INSERÇÃO POR DADOS
app.post("/insert_editora", function(req, res){
  var e = new editora(-1, req.body.nome, req.body.contato, req.body.endereco);
  if(!e.valida()){
    tipo = 1;
    mensagem = "Campo de nome de editora não pode ficar em branco.";  
  }
  else{
    comando = mSQL.editora_incluir(e);
    tipo = 0;
    mensagem = "Inserção da editora de nome " + req.body.nome + " realizada com sucesso!";
  }
  res.render("fim");
});

//ROTAS POST DE EDITORA: ALTERAÇÃO POR ID + DADOS
app.post("/alter_editora", function(req, res){
  if(!req.body.id){
    tipo = 1;
    mensagem = "Campo de id de editora não pode ficar em branco.";
    res.render("fim"); 
  }
  else{
    mSQL.editora_pesquisar_id(req.body.id, function(resultado, campos, comando_sql){
      comando = comando_sql;
      var e = new editora(-1, req.body.nome, req.body.contato, req.body.endereco);
      if(!e.valida()){
        tipo = 1;
        mensagem = "Campo de nome de editora não pode ficar em branco.";  
      }
      else if(JSON.stringify(resultado) == '[]'){
        tipo = 1;
        mensagem = "Editora de id " + req.body.id + " não encontrada no banco de dados.";  
      }
      else{
        comando = mSQL.editora_alterar(e, req.body.id);
        tipo = 0;
        mensagem = "Alteração dos dados da editora de id " + req.body.id + " realizada com sucesso!";
      }
      res.render("fim");
    });
  }
});

//ROTAS POST DE LIVRO: PESQUISA POR TÍTULO
app.post("/select_livro", function(req, res){
  if(!req.body.titulo){
    tipo = 1;
    mensagem = "Campo de título de livro não pode ficar em branco."; 
    res.render("fim"); 
  }
  else{
    mSQL.livro_pesquisar_titulo(req.body.titulo, function(resultado, campos, comando_sql){
      comando = comando_sql;
      tipo = 4;
      mensagem = "Resultado da pesquisa do livro \"" + req.body.titulo + "\":";
      dado = JSON.stringify(resultado);
      res.render("fim"); 
    });
  }
});

//ROTAS POST DE LIVRO: PESQUISA POR AUTOR
app.post("/select_livro_autor", function(req, res){
  if(!req.body.a_nome){
    tipo = 1;
    mensagem = "Campo de nome de autor não pode ficar em branco."; 
    res.render("fim"); 
  }
  else{
    mSQL.livro_pesquisar_autor(req.body.a_nome, function(resultado, campos, comando_sql){
      comando = comando_sql;
      tipo = 4;
      mensagem = "Resultado da pesquisa de livros do autor \"" + req.body.a_nome + "\":";
      dado = JSON.stringify(resultado);
      res.render("fim"); 
    });
  }
});

//ROTAS POST DE LIVRO: PESQUISA POR EDITORA
app.post("/select_livro_editora", function(req, res){
  if(!req.body.e_nome){
    tipo = 1;
    mensagem = "Campo de nome de editora não pode ficar em branco."; 
    res.render("fim"); 
  }
  else{
    mSQL.livro_pesquisar_editora(req.body.e_nome, function(resultado, campos, comando_sql){
      comando = comando_sql;
      tipo = 4;
      mensagem = "Resultado da pesquisa de livros da editora \"" + req.body.e_nome + "\":";
      dado = JSON.stringify(resultado);
      res.render("fim"); 
    });
  }
});

//ROTAS POST DE LIVRO: REMOÇÃO POR ISBN
app.post("/remove_livro", function(req, res){
  if(!req.body.ISBN){
    tipo = 1;
    mensagem = "Campo de ISBN de livro não pode ficar em branco."; 
    res.render("fim"); 
  }
  else{
    mSQL.livro_pesquisar_isbn(req.body.ISBN, function(resultado, campos, comando_sql){
      comando = comando_sql;
      if(JSON.stringify(resultado) == '[]'){
          tipo = 1;
          mensagem = "Livro de ISBN " + req.body.ISBN + " não encontrado no banco de dados.";
      }
      else{
        comando = mSQL.livro_remover(req.body.ISBN);
        tipo = 0;
        mensagem = "Remoção do livro de ISBN " + req.body.ISBN + " realizada com sucesso!";
      }
      res.render("fim");
    });
  }
});

//ROTAS POST DE LIVRO: INSERÇÃO POR DADOS (+PESQUISA NAS TABELAS DE AUTOR E EDITORA PARA PREVINIR DADOS INVÁLIDOS)
app.post("/insert_livro", function(req, res){
  mSQL.livro_pesquisar_isbn(req.body.ISBN, function(resultado, campos, comando_sql){
    comando = comando_sql;
    if(JSON.stringify(resultado) != '[]'){//isbn alterado já existente
      tipo = 1;
      mensagem = "Já existe um livro no banco de dados com o ISBN " + req.body.ISBN + ".";
      res.render("fim");  
    }
    else{
      var l = new livro(req.body.ISBN, req.body.titulo, req.body.data, req.body.preco, 
                        req.body.n_paginas, req.body.descricao, req.body.pais, req.body.id_editora);
      if((!l.valida())||(!req.body.id_autores)){//campo em branco
        tipo = 1;
        mensagem = "Campos obrigatórios não podem ficar em branco.";
        res.render("fim");
      }
      else{
        mSQL.editora_pesquisar_id(req.body.id_editora, function(resultado, campos, comando_sql) {
          comando = comando_sql;
          if(JSON.stringify(resultado) == '[]'){//falha no id de editora
            tipo = 1;
            mensagem = "Editora de id " + req.body.id_editora + " não encontrada no banco de dados.";
            res.render("fim");
          }
          else{
            var str_corrigida = req.body.id_autores.split("\r").join("");
            var str_corrigida = str_corrigida.split("\n").join(" ");
            var str_separada = str_corrigida.split(" ");

            var achou = false;
            for(var i=0; i<str_separada.length; i++){
              for(var j=i+1; j<str_separada.length; j++){
                if(str_separada[i] == str_separada[j]){
                  achou = true;
                  break;
                }
              }
            }
            if(achou){
              tipo = 1;
              mensagem = "Alguns dos ids de autor digitados foram digitados mais de uma vez.";
              res.render("fim");
            }
            else{
              mSQL.autor_pesquisar_varios_ids(str_separada, function(resultado, campos, comando_sql) {
                comando = comando_sql;
                if(resultado.length != str_separada.length){
                  tipo = 1;
                  mensagem = "Alguns dos ids de autor digitados não foram encontrados no banco de dados.";
                  res.render("fim");
                }
                else{//tudo ok
                  comando = mSQL.livro_incluir(l, req.body.ISBN);
                  for(var i=0; i<str_separada.length; i++) 
                    mSQL.escreve_incluir(req.body.ISBN, str_separada[i]);
                  tipo = 0;
                  mensagem = "Inserção do livro de ISBN " + req.body.ISBN + " realizada com sucesso!";
                  res.render("fim");
                }
              });
            }
          }
        });
      }  
    }
  });
});

//ROTAS POST DE LIVRO: ALTERAÇÃO POR ISBN + DADOS (+PESQUISA NAS TABELAS DE AUTOR E EDITORA PARA PREVINIR DADOS INVÁLIDOS)
app.post("/alter_livro", function(req, res){
  if(!req.body.a_ISBN){
    tipo = 1;
    mensagem = "Campo de ISBN de livro de alteração não pode ficar em branco.";
    res.render("fim"); 
  }
  else{
    mSQL.livro_pesquisar_isbn(req.body.ISBN, function(resultado, campos, comando_sql){
      comando = comando_sql;
      if((req.body.a_ISBN != req.body.ISBN)&&(JSON.stringify(resultado) != '[]')){//isbn alterado já existente
        tipo = 1;
        mensagem = "Já existe um livro no banco de dados com o ISBN " + req.body.ISBN + ".";
        res.render("fim");  
      }
      else{
        mSQL.livro_pesquisar_isbn(req.body.a_ISBN, function(resultado, campos, comando_sql){
          comando = comando_sql;
          var l = new livro(req.body.ISBN, req.body.titulo, req.body.data, req.body.preco, 
                            req.body.n_paginas, req.body.descricao, req.body.pais, req.body.id_editora);
          if((!l.valida())||(!req.body.id_autores)){//campo em branco
            tipo = 1;
            mensagem = "Campos obrigatórios não podem ficar em branco.";
            res.render("fim");
          }
          else if(JSON.stringify(resultado) == '[]'){//falha no isbn de procura
            tipo = 1;
            mensagem = "Livro de ISBN " + req.body.a_ISBN + " não encontrado no banco de dados.";
            res.render("fim");
          }
          else{
            mSQL.editora_pesquisar_id(req.body.id_editora, function(resultado, campos, comando_sql) {
              comando = comando_sql;
              if(JSON.stringify(resultado) == '[]'){//falha no id de editora
                tipo = 1;
                mensagem = "Editora de id " + req.body.id_editora + " não encontrada no banco de dados.";
                res.render("fim");
              }
              else{
                var str_corrigida = req.body.id_autores.split("\r").join("");
                var str_corrigida = str_corrigida.split("\n").join(" ");
                var str_separada = str_corrigida.split(" ");

                var achou = false;
                for(var i=0; i<str_separada.length; i++){
                  for(var j=i+1; j<str_separada.length; j++){
                    if(str_separada[i] == str_separada[j]){
                      achou = true;
                      break;
                    }
                  }
                }
                if(achou){
                  tipo = 1;
                  mensagem = "Alguns dos ids de autor digitados foram digitados mais de uma vez.";
                  res.render("fim");
                }
                else{
                  mSQL.autor_pesquisar_varios_ids(str_separada, function(resultado, campos, comando_sql) {
                    comando = comando_sql;
                    if(resultado.length != str_separada.length){
                      tipo = 1;
                      mensagem = "Alguns dos ids de autor digitados não foram encontrados no banco de dados.";
                      res.render("fim");
                    }
                    else{//tudo ok
                      comando = mSQL.livro_alterar(l, req.body.a_ISBN);
                      mSQL.escreve_remover_isbn(req.body.ISBN);
                      for(var i=0; i<str_separada.length; i++)
                        mSQL.escreve_incluir(req.body.ISBN, str_separada[i]);
                      tipo = 0;
                      mensagem = "Alteração dos dados do livro de ISBN " + req.body.a_ISBN + " realizada com sucesso!";
                      res.render("fim");
                    }
                  });
                }
              }
            });
          }
        });
      }
    });
  }
});

//INICIAR SERVIDOR
process.stdout.write("\u001b[2J\u001b[0;0H");
app.listen(8081, function(){
  console.log("Servidor rodando localmente na porta 8081. URL: http://localhost:8081");
});
conectar_banco("livraria");