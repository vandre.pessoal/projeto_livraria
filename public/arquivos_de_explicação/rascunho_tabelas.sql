//COMANDOS PARA CRIAÇÃO DAS TABELAS DO MYSQL
CREATE TABLE editora(
    id_editora INT AUTO_INCREMENT PRIMARY KEY,
    e_nome VARCHAR(50) NOT NULL,
    contato VARCHAR(30),
    endereco VARCHAR(120)
);

CREATE TABLE livro(
    ISBN VARCHAR(20) PRIMARY KEY,
    titulo VARCHAR(50) NOT NULL,
    data_publicacao DATE NOT NULL,
    preco FLOAT NOT NULL,
    num_paginas INT NOT NULL,
    descricao VARCHAR(300),
    pais VARCHAR(30),
    fk_id_editora INT
);

CREATE TABLE autor(
    id_autor INT AUTO_INCREMENT PRIMARY KEY,
    a_nome VARCHAR(50) NOT NULL,
    biografia VARCHAR(600)
);

CREATE TABLE escreve(
    id_relacao INT AUTO_INCREMENT PRIMARY KEY,
    fk_ISBN VARCHAR(20) NOT NULL,
    fk_id_autor INT NOT NULL
);

ALTER TABLE livro ADD CONSTRAINT editora_foreing_key FOREIGN KEY (fk_id_editora) REFERENCES editora(id_editora) ON DELETE SET NULL; 
ALTER TABLE escreve ADD CONSTRAINT ISBN_foreing_key FOREIGN KEY (fk_ISBN) REFERENCES livro(ISBN) ON DELETE CASCADE;
ALTER TABLE escreve ADD CONSTRAINT autor_foreing_key FOREIGN KEY (fk_id_autor) REFERENCES autor(id_autor) ON DELETE CASCADE;


//COMANDOS PARA POPULAR AS TABELAS CRIADAS
INSERT INTO editora(e_nome, contato, endereco) VALUES 
    ("Editora1", "Telefone: 11111111111", "1"),
    ("Editora2", "Telefone: 3188888888", ""),
    ("George Allen & Unwin Ltd.", "", ""),
    ("Paulus", "", ""),
    ("Criativo", "", ""),
    ("Pan Books", "", "");

INSERT INTO autor(a_nome, biografia) VALUES 
    ("Autor 1", "bio 1"),
    ("Autor 2", "bio 2"),
    ("Date,C. J.", ""),
    ("Tolkien,J. R. R.", ""),
    ("Queiruga,Andrés Torres", ""),
    ("Adriano Marangoni", ""),
    ("Bruno Andreotti", ""),
    ("Iberê Moreno", ""),
    ("Mauricio Zanolini", ""),
    ("Douglas Adams", "");

INSERT INTO livro(ISBN, titulo, data_publicacao, preco, num_paginas, descricao, pais, fk_id_editora) VALUES
    ("1",
    "1",
    "2000-1-01",
    1,
    1,
    "1",
    "1",
    1),

    ("8535212736",
    "Introdução à banco de dados",
    "2004-1-01",
    243.90,
    896,
    "Livro didático de introdução bancos de dados com comandos SQL. 8º edição.",
    "Brasil",
    1),

    ("9788595084742",
    "O Hobbit",
    "1937-7-21",
    99.99,
    336,
    "Best seller.",
    "Brasil",
    3),

    ("9788534909082",
    "O diálogo das religiões",
    "2007-1-01",
    9.90,
    81,
    "Livro didático sobre os temas de ensino religioso e sociologia.",
    "Brasil",
    4),

    ("9788582580639",
    "Os Dois Lados da Guerra Civil",
    "2016-3-16",
    9.99,
    72,
    "Uma visão filosófica da obra de ficção Guerra Civil. Vários autores.",
    "Brasil",
    5),

    ("9788599296578",
    "O guia do mochileiro das galáxias v1",
    "2011-1-01",
    30.00,
    208,
    "Livro best seller. Livre para todos os públicos",
    "Brasil",
    6),
    
    ("9788580415544",
    "O Guia Definitivo do Mochileiro Das Galáxias",
    "2016-1-01",
    56.50,
    672,
    "Livro best seller. Livre para todos os públicos",
    "Brasil",
    6);

INSERT INTO escreve(fk_ISBN, fk_id_autor) VALUES
    ("1", 1),
    ("1", 2),
    ("8535212736", 3),
    ("9788595084742", 4),
    ("9788534909082", 5),
    ("9788582580639", 6),
    ("9788582580639", 7),
    ("9788582580639", 8),
    ("9788582580639", 9),
    ("9788599296578", 10),
    ("9788580415544", 10);
    
