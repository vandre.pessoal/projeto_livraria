module.exports = class livro{
    constructor(ISBN, Titulo, Data_Publicacao, Preco, N_Paginas, Descricao, Pais, Id_Editora){
        this.ISBN = ISBN;
        this.titulo = Titulo;
        this.data_publicacao = Data_Publicacao;
        this.preco = Preco;
        this.n_paginas = N_Paginas;
        this.descricao = Descricao;
        this.pais = Pais;
        this.id_editora = Id_Editora;
    }
    valida(){
        return(
            (!(!this.ISBN))&&
            (!(!this.titulo))&&
            (!(!this.data_publicacao))&&
            (!(!this.preco))&&
            (!(!this.id_editora))&&
            (!(!this.n_paginas))
            );
    }
}