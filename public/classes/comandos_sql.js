module.exports = class comandos_sql{
    constructor(Banco){
        this.banco = Banco;
    }
    //CHAMADA DOS COMANDOS SQL
    chama_comando(comando_sql, callback){
        this.banco.query(comando_sql, function (err, resposta, campos) {
        if(err) throw err;
        if(callback != null) return callback(resposta, campos, comando_sql);
        });
    }
  
  /*CHAMADAS SQL NA TABELA AUTOR*/
    autor_incluir(a){
        var comando_sql = "INSERT INTO autor(a_nome, biografia) VALUES \n(\"" + a.nome + "\", \"" + a.biografia + "\");";
        this.chama_comando(comando_sql, null);
        return comando_sql;
    }
  
    autor_remover(id){
        var comando_sql = "DELETE FROM autor \nWHERE id_autor = \"" + id + "\";";
        this.chama_comando(comando_sql, null);
        return comando_sql;
    }
  
    autor_alterar(a, id){
        var comando_sql = "UPDATE autor SET a_nome = \"" + a.nome + "\", biografia = \"" + a.biografia + "\" \nWHERE id_autor = \"" + id + "\";";
        this.chama_comando(comando_sql, null);
        return comando_sql;
    }
  
    autor_pesquisar_id(id, callback){
        var comando_sql = "SELECT * FROM autor \nWHERE id_autor = \"" + id + "\";";
        return this.chama_comando(comando_sql, callback);
    }
  
    autor_pesquisar_varios_ids(lista_ids, callback){
        var comando_sql = "SELECT * FROM autor \nWHERE id_autor = \"" + lista_ids[0] + "\"";
        for(var i=1; i<lista_ids.length; i++){
        comando_sql = comando_sql + (" \nOR id_autor = \"" + lista_ids[i] + "\"");
        }
        comando_sql = comando_sql + (";");
        console.log(comando_sql);
        return this.chama_comando(comando_sql, callback);
    }
  
    autor_pesquisar_nome(nome, callback){
        var comando_sql = "SELECT * FROM autor \nWHERE a_nome = \"" + nome + "\";";
        return this.chama_comando(comando_sql, callback);
    }
  
  /*CHAMADAS SQL NA TABELA EDITORA*/
    editora_incluir(e){
        var comando_sql = "INSERT INTO editora(e_nome, contato, endereco) VALUES \n(\"" + e.nome + "\", \"" + e.contato + "\", \"" + e.endereco + "\");";
        this.chama_comando(comando_sql, null);
        return comando_sql;
    }
  
    editora_remover(id){
        var comando_sql = "DELETE FROM editora \nWHERE id_editora = \"" + id + "\";";
        this.chama_comando(comando_sql, null);
        return comando_sql;
    }
  
    editora_alterar(e, id){
        var comando_sql = "UPDATE editora SET e_nome = \"" + e.nome + "\", contato = \"" + e.contato + "\", endereco = \"" + e.endereco + "\" \nWHERE id_editora = \"" + id + "\";";
        this.chama_comando(comando_sql, null);
        return comando_sql;
    }
  
    editora_pesquisar_id(id, callback){
        var comando_sql = "SELECT * FROM editora \nWHERE id_editora = \"" + id + "\";";
        return this.chama_comando(comando_sql, callback);
    }
  
    editora_pesquisar_nome(nome, callback){
        var comando_sql = "SELECT * FROM editora \nWHERE e_nome = \"" + nome + "\";";
        return this.chama_comando(comando_sql, callback);
    }
  
  
  /*CHAMADAS SQL NA TABELA LIVRO*/
    livro_incluir(l){
        var comando_sql = "INSERT INTO livro(ISBN, titulo, data_publicacao, preco, num_paginas, descricao, pais, fk_id_editora) VALUES \n(\"" + l.ISBN + "\", \"" + l.titulo + "\", \"" + l.data_publicacao + "\", \"" + l.preco + "\",\"" + l.n_paginas + "\",\"" + l.descricao + "\",\"" + l.pais + "\",\"" + l.id_editora + "\");";
        this.chama_comando(comando_sql, null);
        return comando_sql;
    }
  
    livro_remover(ISBN){
        var comando_sql = "DELETE FROM livro \nWHERE ISBN = \"" + ISBN + "\";";
        this.chama_comando(comando_sql, null);
        return comando_sql;
    }
  
    livro_alterar(l, ISBN){
        var comando_sql = "UPDATE livro SET ISBN = \"" + l.ISBN + "\", titulo = \"" + l.titulo + "\", data_publicacao = \"" + l.data_publicacao + "\", preco = \"" + l.preco + "\", num_paginas = \"" + l.n_paginas + "\", descricao = \"" + l.descricao + "\", pais = \"" + l.pais + "\", fk_id_editora = \"" + l.id_editora + "\" \nWHERE ISBN = \"" + ISBN + "\";";
        this.chama_comando(comando_sql, null);
        return comando_sql;
    }
  
    livro_pesquisar_isbn(ISBN, callback){
        var comando_sql = "SELECT * FROM livro \nWHERE ISBN = \"" + ISBN + "\";";
        return this.chama_comando(comando_sql, callback);
    }
  
  
  /*CHAMADAS SQL NA TABELA ESCREVE*/
    escreve_incluir(ISBN, id_autor){
        var comando_sql = "INSERT INTO escreve(fk_ISBN, fk_id_autor) VALUES \n(\"" + ISBN + "\", \"" + id_autor + "\");";
        this.chama_comando(comando_sql, null);
        return comando_sql;
    }
  
    escreve_remover_isbn(ISBN){
        var comando_sql = "DELETE FROM escreve \nWHERE fk_ISBN = \"" + ISBN + "\";";
        this.chama_comando(comando_sql, null);
        return comando_sql;
    }
  
    escreve_remover_match(ISBN, id_autor){
        var comando_sql = "DELETE FROM escreve \nWHERE fk_ISBN = \"" + ISBN + "\" AND fk_id_autor = \"" + id_autor + "\";";
        this.chama_comando(comando_sql, null);
        return comando_sql;
    }
  
    escreve_alterar_autor(ISBN, id_autor){
        var comando_sql = "UPDATE escreve SET fk_id_autor = \"" + id_autor + "\" \nWHERE fk_ISBN = \"" + ISBN + "\";";
        this.chama_comando(comando_sql, null);
        return comando_sql;
    }
  
  /*CHAMADAS SQL PARA PESQUISAR NA TABELA LIVRO*/
    livro_pesquisar_titulo(titulo, callback){
        var comando_sql = "SELECT A.ISBN, A.titulo, A.data_publicacao, A.preco, A.num_paginas, A.descricao, A.pais, A.e_nome, B.a_nome \nFROM escreve C JOIN (SELECT A.ISBN, A.titulo, A.data_publicacao, A.preco, A.num_paginas, A.descricao, A.pais, D.e_nome \nFROM livro A JOIN editora D ON D.id_editora = A.fk_id_editora) A ON A.ISBN = C.fk_ISBN \nJOIN autor B ON B.id_autor = C.fk_id_autor ";
        comando_sql = comando_sql + ("\nWHERE A.titulo = \"" + titulo + "\" ORDER BY A.ISBN;");
        return this.chama_comando(comando_sql, callback);
    }
  
    livro_pesquisar_autor(nome_autor, callback){
        var comando_sql = "SELECT A.ISBN, A.titulo, A.data_publicacao, A.preco, A.num_paginas, A.descricao, A.pais, A.e_nome, B.a_nome \nFROM escreve C JOIN (SELECT A.ISBN, A.titulo, A.data_publicacao, A.preco, A.num_paginas, A.descricao, A.pais, D.e_nome \nFROM livro A JOIN editora D ON D.id_editora = A.fk_id_editora) A ON A.ISBN = C.fk_ISBN \nJOIN autor B ON B.id_autor = C.fk_id_autor ";
        comando_sql = comando_sql + ("\nWHERE A.ISBN IN (SELECT A.ISBN \nFROM escreve C JOIN livro A ON A.ISBN = C.fk_ISBN \nJOIN autor B ON B.id_autor = C.fk_id_autor ");
        comando_sql = comando_sql + ("\nWHERE B.a_nome = \"" + nome_autor + "\" ORDER BY A.ISBN) ORDER BY A.ISBN;");
        return this.chama_comando(comando_sql, callback);
    }
  
    livro_pesquisar_editora(nome_editora, callback){
        var comando_sql = "SELECT A.ISBN, A.titulo, A.data_publicacao, A.preco, A.num_paginas, A.descricao, A.pais, A.e_nome, B.a_nome \nFROM escreve C JOIN (SELECT A.ISBN, A.titulo, A.data_publicacao, A.preco, A.num_paginas, A.descricao, A.pais, D.e_nome \nFROM livro A JOIN editora D ON D.id_editora = A.fk_id_editora) A ON A.ISBN = C.fk_ISBN \nJOIN autor B ON B.id_autor = C.fk_id_autor ";
        comando_sql = comando_sql + ("\nWHERE A.ISBN IN (SELECT A.ISBN \nFROM escreve C JOIN livro A ON A.ISBN = C.fk_ISBN \nJOIN autor B ON B.id_autor = C.fk_id_autor ");
        comando_sql = comando_sql + ("\nWHERE A.e_nome = \"" + nome_editora + "\" ORDER BY A.ISBN) ORDER BY A.ISBN;");
        return this.chama_comando(comando_sql, callback);
    }
}